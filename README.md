# Altran Backend Assesment

## Usage

1. Clone the project
1. npm install on the directory
1. run ```node server/index``` or ```nodemon server```

The system won't work without any users created in the database where it's connecting.  
I've made a test DB with two users (one with admin and the other with user role) to simplify testing.  
In order to connect to the DB, create a ```.env``` file in the root of the project with the following info:

```env
PORT=<The port you wish to run the app in, i.e 8080>
DB_URL=mongodb://backendtest:backendtest123@ds161112.mlab.com:61112/backendtest
SECRET=<Secret phrase used to hash the passwords, i.e B9776D7DDF459C9AD5B0E1D6AC61E27BEFB>
```

After that, you can login (check the routes below) using any of these users (or feel free to create yours):

|Username|Password|Role |
|---	|---	|---	|
|ADMIN  |admin  |"admin"|
|USER   |user   |"user" |

## Routes

BASE_URL: {PROTOCOL}{HOST}/api
i.e _http://localhost:8080/api_

The server has four types of routes (Auth, Client, Policies and User routes).

### Auth: Login service

```Route Name```: __Login__  
```Route```: {BASE_URL}/auth/login  
```Method```: POST  
```Headers```: NONE  
```Access Level```: Unauthenticated access (no auth_token)  
```Example payload```:  
```js
{
    'username': 'ADMIN',
    'password': 'admin'
}
```  

```Example response```:  
```js
{
    "status": 200,
    "body": {
        "_id": "5ba1250bd6951b04dc250343",
        "user_id": "5ba124f1b541632d842fdf88",
        "username": "ADMIN",
        "role": "admin",
        "auth_token": "6c87a013-e8e8-4171-9fc7-98d996c6a490",
        "created_at": "2018-09-18T00:00:00.000Z",
        "exp_date": "2018-09-19",
        "__v": 0
    },
    "message": "Logged in succesfully"
}
```  

The auth_token will be needed to use any of the other routes. Some of them will be accessible to Admins only or both Admins and Users depending on the role the user has.    
To use it, simply include it in the headers of your HTTP request as 'auth_token=<token_value>'.  

## Client Routes

```Route Name```: __Get client data by client ID__     
```Route```: {BASE_URL}/clients/id/{CLIENT_ID}  
```Method```: GET  
```Headers```: auth_token=<AUTH_TOKEN retrieved on LOGIN>  
```Access Level```: Admin, User  
```Example payload```:  
```
GET http://localhost:8080/api/clients/id/a0ece5db-cd14-4f21-812f-966633e7be86
```

```Example response```:  
```js
{
    "status": 200,
    "body": [
        {
            "id": "a0ece5db-cd14-4f21-812f-966633e7be86",
            "name": "Britney",
            "email": "britneyblankenship@quotezart.com",
            "role": "admin"
        }
    ],
    "message": "Client data for ID [a0ece5db-cd14-4f21-812f-966633e7be86] retrieved successfully."
}
```


```Route Name```: __Get client data by client email__  
```Route```: {BASE_URL}/clients/email/<CLIENT_EMAIL>  
```Method```: GET  
```Headers```: auth_token=<AUTH_TOKEN retrieved on LOGIN>  
```Access Level```: Admin, User  
```Example payload```:
```
GET http://localhost:8080/api/clients/email/britneyblankenship@quotezart.com
```

```Example response```:  
```js
{
    "status": 200,
    "body": [
        {
            "id": "a0ece5db-cd14-4f21-812f-966633e7be86",
            "name": "Britney",
            "email": "britneyblankenship@quotezart.com",
            "role": "admin"
        }
    ],
    "message": "Client data for email [britneyblankenship@quotezart.com] retrieved successfully."
}
```


## Policy routes

```Route Name```: __Get policies by client email__  
```Route```: {BASE_URL}/policies/email/<CLIENT_EMAIL>  
```Method```: GET  
```Headers```: auth_token=<AUTH_TOKEN retrieved on LOGIN>  
```Access Level```: Admin  
```Example payload```:
```
GET http://localhost:8080/api/policies/email/inesblankenship@quotezart.com
```

```Example response```:
```js
{
    "status": 200,
    "body": [
        {
            "id": "64cceef9-3a01-49ae-a23b-3761b604800b",
            "amountInsured": 1825.89,
            "email": "inesblankenship@quotezart.com",
            "inceptionDate": "2016-06-01T03:33:32Z",
            "installmentPayment": true,
            "clientId": "e8fd159b-57c4-4d36-9bd7-a59ca13057bb"
        },
        {
            "id": "7b624ed3-00d5-4c1b-9ab8-c265067ef58b",
            "amountInsured": 399.89,
            "email": "inesblankenship@quotezart.com",
            "inceptionDate": "2015-07-06T06:55:49Z",
            "installmentPayment": true,
            "clientId": "a0ece5db-cd14-4f21-812f-966633e7be86"
        }
    ],
    "message": "Policies for client email [inesblankenship@quotezart.com] retrieved successfully."
}
```


```Route Name```: __Get client data by policy ID__    
```Route```: {BASE_URL}/policies/policy_id/<POLICY_ID>  
```Method```: GET  
```Headers```: auth_token=<AUTH_TOKEN retrieved on LOGIN>  
```Access Level```: Admin  
```Example payload```:
```
GET http://localhost:8080/api/policies/policy_id/0eba1179-6155-41b5-bdd8-f80e1ac94cab
```

```Example response```:
```js
{
    "status": 200,
    "body": [
        {
            "id": "0eba1179-6155-41b5-bdd8-f80e1ac94cab",
            "amountInsured": 1629.72,
            "email": "inesblankenship@quotezart.com",
            "inceptionDate": "2016-05-03T01:53:34Z",
            "installmentPayment": false,
            "clientId": "e8fd159b-57c4-4d36-9bd7-a59ca13057bb",
            "clientData": {
                "id": "e8fd159b-57c4-4d36-9bd7-a59ca13057bb",
                "name": "Manning",
                "email": "manningblankenship@quotezart.com",
                "role": "admin"
            }
        }
    ],
    "message": "Client data related to policy ID [0eba1179-6155-41b5-bdd8-f80e1ac94cab] retrieved successfully."
}
```

## User routes

Used to manage users in the system.  

```Route Name```: __Create new user__      
```Route```: {BASE_URL}/users/new    
```Method```: POST    
```Headers```: auth_token=<AUTH_TOKEN retrieved on LOGIN>  
```Access Level```: Admin    
```Example payload```:
```
{
	"username": "ADMIN",
	"password": "123",
	"role": "admin"
}
```

```Example response```:
```js
{
    "status": 200,
    "body": {
        "_id": "5ba12500b541632d842fdf89",
        "username": "ADMIN",
        "password": "c15ccff4185c7266bb96e1e8f85523ab8d826bb6b118f1f9af6f26c96166830a",
        "role": "admin",
        "__v": 0
    },
    "message": "User [ADMIN] with role [admin] was created successfully."
}
```