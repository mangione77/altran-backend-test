const HTTP = require('../constants/HTTP')
const responseHandler = {}

responseHandler.onSuccess = (_body, _message) => {
    return { status: HTTP.SUCCESS, body: _body, message: _message }
}

responseHandler.onFailure = (_status,_message,_stacktrace) => {
    return { status: _status, message: _message, stacktrace: _stacktrace }
}

module.exports = responseHandler