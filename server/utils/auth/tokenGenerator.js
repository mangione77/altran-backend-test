const uuid = require('uuid/v4')
const moment = require('moment')
const Token = require('../../models/Token')

const tokenGenerator = async (user_id,username,role,auth_token,created_at,exp_date) => {
    try {
        let newToken = new Token({
            user_id: user_id,
            username: username,
            role: role,
            auth_token: auth_token,
            created_at: created_at,
            exp_date: exp_date
        })
        let newSavedToken = await newToken.save()
        return newSavedToken
    }
    catch (err) {
        throw err
    }
}

module.exports = tokenGenerator
