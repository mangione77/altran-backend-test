const User = require('../../models/User')
const HTTP = require('../../constants/HTTP')

const authenticateRole = async (accessLevel,userId) => {
    try {   
        let foundUser = await User.findOne({ '_id': userId })
        if (foundUser !== null) {
            if (accessLevel.includes(foundUser.role)) {
                return ({ status: HTTP.SUCCESS, message: 'Access level is valid.'})
            }
            else {
                return ({ status: HTTP.FORBIDDEN, message: 'Forbidden.' })
            }
        }
        else {
            return ({ status: HTTP.NOT_FOUND, message: `User with ID ${userId} was not found.` })
        }
    }
    catch (err) {
        throw (err)
    }
}

module.exports = authenticateRole