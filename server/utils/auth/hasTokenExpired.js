const moment = require('moment')

const hasTokenExpired = (exp_date) => {
    let now = moment(+new Date()).format('YYYY-MM-DD')
    let diff = moment(now).diff(exp_date)
    let isTokenExpired
    diff <= 0 ? isTokenExpired = false : isTokenExpired = true
    return isTokenExpired
}

module.exports = hasTokenExpired
