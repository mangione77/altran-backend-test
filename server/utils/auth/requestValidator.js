const validateToken = require('./validateToken')
const validateUserRole = require('./validateUserRole')
const { onFailure } = require('../responseHandler')
const ERRORS = require('../../constants/ERRORS')
const HTTP = require('../../constants/HTTP')

const requestValidator = (role)  => {
    return async (req,res,next) => {
        let authToken = req.headers.auth_token
        if (authToken === undefined) {
            res.send({ status: HTTP.FORBIDDEN, message: 'Supplied token is invalid.' })
        }
        let tokenValidation = await validateToken(authToken)
        if (tokenValidation.status === 200) {
            let userRoleValidation = await validateUserRole(role,tokenValidation.body.user_id)
            if (userRoleValidation.status === 200) {
                next()
            }
            else {
                res.send(onFailure(userRoleValidation.status, ERRORS.VALIDATING_USER_ROLE, userRoleValidation.message))
            }
        }
        else {
            res.send(onFailure(tokenValidation.status, ERRORS.VALIDATING_TOKEN, tokenValidation.message))
        }
    }
}

module.exports = requestValidator