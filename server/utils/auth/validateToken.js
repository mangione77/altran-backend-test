const Token = require('../../models/Token')
const hasTokenExpired = require('../auth/hasTokenExpired')
const HTTP = require('../../constants/HTTP')

const validateToken =  async (auth_token) => {
    try {
        let foundToken = await Token.findOne({ 'auth_token': auth_token })
        if (foundToken !== null) {
            if (!hasTokenExpired(foundToken.exp_date)) {
                return ({ status: HTTP.SUCCESS, body: foundToken, message: 'Supplied token is valid' })
            }
            else {
                return ({ status: HTTP.FORBIDDEN, body: false, message: 'Supplied token has expired. Log in and try again.' })
            }
        }
        else {
            return ({ status: HTTP.NOT_FOUND, body: false, message: 'Supplied token was not found' })
        }
    }
    catch (err) {
        throw(err)
    }
}

module.exports = validateToken