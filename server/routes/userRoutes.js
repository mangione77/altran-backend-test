const express = require('express')
const userRoutes = express()
const userController = require('../controllers/userController')
const accessLevel = require('../utils/auth/requestValidator')

userRoutes.post('/new', accessLevel(['admin']), userController.newUser)

module.exports = userRoutes