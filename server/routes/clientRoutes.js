const express = require('express')
const clientRoutes = express()
const accessLevel = require('../utils/auth/requestValidator')
const clientController = require('../controllers/clientController')

clientRoutes.get('/id/:userId', accessLevel(['admin', 'user']), clientController.getClientDataByClientId)
clientRoutes.get('/email/:email', accessLevel(['admin', 'user']), clientController.getClientDataByEmail)

module.exports = clientRoutes