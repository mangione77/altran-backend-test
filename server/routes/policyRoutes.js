const express = require('express')
const policyRoutes = express()
const policyController = require('../controllers/policyController')
const accessLevel = require('../utils/auth/requestValidator')

policyRoutes.get('/email/:email', accessLevel(['admin']), policyController.getPoliciesByClientEmail)
policyRoutes.get('/policy_id/:policyId', accessLevel(['admin']), policyController.getClientDataByPolicyId)

module.exports = policyRoutes