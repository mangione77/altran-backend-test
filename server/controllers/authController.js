const authService = require('../services/authService')
const HTTP = require('../constants/HTTP')
const ERRORS = require('../constants/ERRORS')
const MESSAGES = require('../constants/MESSAGES') 
const { onSuccess, onFailure } = require('../utils/responseHandler')
const CryptoJS = require('crypto-js')
const authController = {}

authController.login = async (req,res) => {
    try {
        let { username, password } = req.body
        password = CryptoJS.HmacSHA256(password, process.env.SECRET).toString()
        let loginResponse = await authService.login(username,password)
        res.send(onSuccess(loginResponse, MESSAGES.LOGGED_IN))
    }
    catch (err) {
        res.send(onFailure(err.status, ERRORS.LOGGING_IN, err.message))
    }
}

module.exports = authController