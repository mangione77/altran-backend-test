const userService = require('../services/userService')
const userController = {}
const { onSuccess, onFailure } = require('../utils/responseHandler')
const ERRORS = require('../constants/ERRORS')
const MESSAGES = require('../constants/MESSAGES')
const CryptoJS = require('crypto-js')

userController.newUser = async (req,res) => {
    let { username, password, role } = req.body
    try {
        password = CryptoJS.HmacSHA256(password, process.env.SECRET)
        let newUser = await userService.newUser(username,password,role)
        res.send(onSuccess(newUser, MESSAGES.USER_CREATED(username,role)))
    }
    catch (err) {
        res.send(onFailure(err.status, ERRORS.CREATING_USER, err.message))
    }
}

module.exports = userController