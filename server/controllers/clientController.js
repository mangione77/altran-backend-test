const clientService = require('../services/clientService')
const clientController = {}
const { onSuccess, onFailure } = require('../utils/responseHandler')
const MESSAGES = require('../constants/MESSAGES')
const ERRORS = require('../constants/ERRORS')

clientController.getClientDataByClientId = async (req,res) => {
    let { userId } = req.params
    try {
        let userData = await clientService.getClientDataByClientId(userId)
        res.send(onSuccess(userData, MESSAGES.CLIENT_DATA_BY_ID(userId)))
    }
    catch (err) {
        res.send(onFailure(err.status, ERRORS.GETTING_CLIENT_DATA_BY_ID(userId), err.message))
    }
}

clientController.getClientDataByEmail = async (req,res) => {
    let { email } = req.params
    try {
        let userData = await clientService.getClientDataByEmail(email)
        res.send(onSuccess(userData, MESSAGES.CLIENT_DATA_BY_EMAIL(email)))
    }
    catch (err) {
        res.send(onFailure(err.status, ERRORS.GETTING_CLIENT_DATA_BY_EMAIL(email), err.message))
    }
}

module.exports = clientController