const policyService = require('../services/policyService')
const { onSuccess, onFailure } = require('../utils/responseHandler')
const MESSAGES = require('../constants/MESSAGES')
const ERRORS = require('../constants/ERRORS')
const policyController = {}

policyController.getPoliciesByClientEmail = async (req,res) => {
    let { email } = req.params
    try {
        let policyData = await policyService.getPoliciesByClientEmail(email)
        res.send(onSuccess(policyData, MESSAGES.POLICY_DATA_BY_EMAIL(email)))
    }
    catch (err) {
        res.send(onFailure(err.status, ERRORS.GETTING_POLICY_DATA_BY_EMAIL(email), err.message))
    }
}

policyController.getClientDataByPolicyId = async (req,res) => {
    let { policyId } = req.params
    try {
        let policyData = await policyService.getClientDataByPolicyId(policyId)
        res.send(onSuccess(policyData, MESSAGES.CLIENT_DATA_BY_POLICY(policyId)))
    }
    catch (err) {
        res.send(onFailure(err.status, ERRORS.GETTING_CLIENT_DATA_BY_POLICY(policyId), err.message))
    }
}

module.exports = policyController