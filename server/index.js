require('dotenv').config()
const express = require('express')
const app = express()
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const { PORT } = process.env
const userRoutes = require('./routes/userRoutes')
const authRoutes = require('./routes/authRoutes')
const clientRoutes = require('./routes/clientRoutes')
const policyRoutes = require('./routes/policyRoutes')

mongoose.connect(process.env.DB_URL, { useNewUrlParser: true }, (err) => {
    if (err) console.log('Error connecting to DB: ', err)

    console.log('Connected to DB.')
})

app.use(bodyParser.json())
app.use('/api/auth', authRoutes)
app.use('/api/users', userRoutes)
app.use('/api/clients', clientRoutes)
app.use('/api/policies', policyRoutes)

app.listen(PORT, () => {
    console.log(`Listening on port ${PORT}`)
})