const mongoose = require('mongoose')
const Schema = mongoose.Schema

const Token = new Schema({
    user_id: { type: String, required: true },
    username: { type: String, required: true },
    role: { type: String, required: true },
    auth_token: { type: String, required: true },
    created_at: { type: Date, required: true },
    exp_date: { type: String, required: true }
})

module.exports = mongoose.model('Token', Token)
