const User = require('../models/User')
const userService = {}

userService.newUser = async (username,password,role) => {
    try {
        let user = new User({ username, password, role })
        let newUser = await user.save()
        return newUser
    }
    catch (err) {
        throw(err)
    }
}

module.exports = userService