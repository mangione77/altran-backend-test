const axios = require('axios')
const clientService = require('./clientService')
const HTTP = require('../constants/HTTP')
const policyService = {}

policyService.getPoliciesByClientEmail = async (email) => {
    try {
        let apiResponse = await axios.get('http://www.mocky.io/v2/580891a4100000e8242b75c5')
        let policyData = apiResponse.data.policies
        let foundPolicies = policyData.filter(policy => {
            return policy.email === email
        })
        if (foundPolicies.length > 0) {
            return foundPolicies
        }
        else {
            throw ({ status: HTTP.NOT_FOUND, message: `No policies linked to email ${email}`})
        }
    }
    catch (err) {
        throw (err)
    }
}

policyService.getClientDataByPolicyId = async (policyId) => {
    try {
        let apiResponse = await axios.get('http://www.mocky.io/v2/580891a4100000e8242b75c5')
        let policyData = apiResponse.data.policies
        let foundPolicy = policyData.filter(policy => {
            return policy.id === policyId
        })
        if (foundPolicy.length > 0) {
            let clientData = await clientService.getClientDataByClientId(foundPolicy[0].clientId)
            foundPolicy[0].clientData = clientData[0]
            return foundPolicy
        }
        else {
            throw ({ status: HTTP.NOT_FOUND, message: `No policy with ID ${policyId} was found` })
        }
    }
    catch (err) {
        throw (err)
    }
}

module.exports = policyService