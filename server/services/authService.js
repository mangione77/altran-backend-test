const User = require('../models/User')
const tokenGenerator = require('../utils/auth/tokenGenerator')
const HTTP = require('../constants/HTTP')
const uuid = require('uuid/v4')
const moment = require('moment')
const authService = {}

authService.login = async (username,password) => {
    try {
        let foundUser = await User.findOne({ 'username': username, 'password': password })
        if (foundUser !== null) {
            let created_at = moment(+new Date()).format('YYYY-MM-DD')
            let exp_date = moment(created_at).add(1, 'days').format('YYYY-MM-DD')
            let auth_token = uuid()
            let newToken = await tokenGenerator(foundUser._id, foundUser.username, foundUser.role, auth_token, created_at, exp_date)
            return newToken
        }
        else {
            throw({ status: HTTP.NOT_FOUND, message: 'User not found. Check username and/or password' })
        }
    }
    catch (err) {
        throw(err)
    }
}

module.exports = authService