const axios = require('axios')
const HTTP = require('../constants/HTTP')
const clientService = {}

clientService.getClientDataByClientId = async (userId) => {
    try {
        let apiResponse = await axios.get('http://www.mocky.io/v2/5808862710000087232b75ac')
        let clientData = apiResponse.data.clients
        let foundUserData = clientData.filter(client => {
            return client.id === userId
        })
        if (foundUserData.length > 0) {
            return foundUserData
        }
        else {
            throw ({ status: HTTP.NOT_FOUND, message: `No user was found with ID ${userId}` })
        }
    }
    catch (err) {
        throw (err)
    }
}

clientService.getClientDataByEmail = async (email) => {
    try {
        let apiResponse = await axios.get('http://www.mocky.io/v2/5808862710000087232b75ac')
        let clientData = apiResponse.data.clients
        let foundUserData = clientData.filter(client => {
            return client.email === email
        })
        if (foundUserData.length > 0) {
            return foundUserData
        }
        else {
            throw ({ status: HTTP.NOT_FOUND, message: `No client was found with email ${email}` })
        }
    }
    catch (err) {
        throw (err)
    }
}

module.exports = clientService