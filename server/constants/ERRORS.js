module.exports = class HTTP {
    static get LOGGING_IN() { return 'Something went wrong logging in...' }
    static get VALIDATING_USER_ROLE() { return 'Check your User role & access level.'}
    static get VALIDATING_TOKEN() { return 'Check your auth_token.'}
    static get CREATING_USER() { return 'Something went wrong while creating user...'}
    static GETTING_CLIENT_DATA_BY_ID(clientId) { return `Something went wrong retrieving client data for ID [${clientId}]...`}
    static GETTING_CLIENT_DATA_BY_EMAIL(clientEmail) { return `Something went wrong retrieving client data for email [${clientEmail}]...`}
    static GETTING_POLICY_DATA_BY_EMAIL(clientEmail) { return `Something went wrong retrieving policies data for client [${clientEmail}]...`}
    static GETTING_CLIENT_DATA_BY_POLICY(policyId) { return `Something went wrong retrieving client data related to policy ID [${policyId}]...` }
}
