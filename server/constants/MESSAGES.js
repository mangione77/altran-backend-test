module.exports = class HTTP {
    static get LOGGED_IN() { return 'Logged in succesfully' }
    static CLIENT_DATA_BY_ID(clientId) { return `Client data for ID [${clientId}] retrieved successfully.` }
    static CLIENT_DATA_BY_EMAIL(clientEmail) { return `Client data for email [${clientEmail}] retrieved successfully.` }
    static POLICY_DATA_BY_EMAIL(clientEmail) { return `Policies for client email [${clientEmail}] retrieved successfully.` }
    static CLIENT_DATA_BY_POLICY(policyId) { return `Client data related to policy ID [${policyId}] retrieved successfully.` }
    static USER_CREATED(username,role) { return `User [${username}] with role [${role}] was created successfully.` }
}
