module.exports = class HTTP {
    static get SUCCESS() { return 200 }
    static get NOT_FOUND() { return 404 }
    static get INVALID() { return 400 }
    static get FORBIDDEN() { return 403 }
    static get NOT_ACCEPTABLE() { return 406 }
    static get SERVER_ERROR() { return 500 }
    static get SESSION_LOCKED() { return 423 }
}
